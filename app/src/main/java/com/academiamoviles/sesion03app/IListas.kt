package com.academiamoviles.sesion03app

import androidx.appcompat.app.AlertDialog

interface IListas {

    fun createLoginDialogo(nombre:String,anio:String,descripcion:String,marca:String): AlertDialog
}