package com.academiamoviles.sesion03app

import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialogo.*
import java.util.*


class MainActivity : AppCompatActivity() , IListas {

    var marcaElegida = ""
    var contador = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val marcas = listOf("Kia", "Hyundai","Toyota","Nizzan","Fiat","Subaru")

        val adapter = ArrayAdapter(this, R.layout.style_spinner, marcas)
        spMarcas.adapter = adapter

        spMarcas.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {

                if (contador!=0){
                    //createLoginDialogo(marcas[position]).show()
                    marcaElegida = marcas[position]
                }
                contador++

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // write code to perform some action
            }
        }

        btnVerificar.setOnClickListener {

            if (marcaElegida.isEmpty()){
                Toast.makeText(this,"Debe elegir una marca",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val nombre = edtNombres.text.toString()
            val anio = edtAnio.text.toString()
            val descripcion = edtDescripcion.text.toString()

            val persona = Vehiculo(nombre,anio,descripcion,marcaElegida)

            createLoginDialogo(nombre,anio,descripcion,marcaElegida).show()


            //tvResultado.text = "Estimado ${persona.nombre}. Usted ha traído su vehículo de marca: ${persona.marca}. Del año: ${persona.anio}" +
            //        ". Con las siguiente descripción: ${persona.descripcion}"
        }

    }

    override fun createLoginDialogo(nombre:String, anio:String, descripcion:String, marca:String): AlertDialog {
        val alertDialog: AlertDialog
        val builder  = AlertDialog.Builder(this)
        val inflater = layoutInflater
        val v: View = inflater.inflate(R.layout.dialogo, null)
        builder.setView(v)
        val btnAperturarNo: Button = v.findViewById(R.id.btn_aperturar_no)
        val btnAperturarSi: Button = v.findViewById(R.id.btn_aperturar_si)
        val imgLogo: ImageView = v.findViewById(R.id.imgLogo)
        val tvDatos: TextView = v.findViewById(R.id.tvDatos)

        if (marca == "Honda") {
            imgLogo.setBackgroundResource(R.drawable.icon_kia)
        }else if(marca == "Hyundai") {
            imgLogo.setBackgroundResource(R.drawable.icon_hyundai)
        }else if(marca == "Toyota"){
            imgLogo.setBackgroundResource(R.drawable.icon_toyota)
        }else if(marca == "Chevrolet"){
            imgLogo.setBackgroundResource(R.drawable.icon_nizzan)
        }else if(marca == "Fiat"){
            imgLogo.setBackgroundResource(R.drawable.icon_fiat)
        }else if(marca == "Subaru"){
            imgLogo.setBackgroundResource(R.drawable.icon_subaru)
        }

        tvDatos.text = "Propietario: $nombre \nMarca del vehículo: $marca \nAño: $anio \nProblema: $descripcion"

        alertDialog = builder.create()

        btnAperturarSi.setOnClickListener {
            marcaElegida = marca
            Toast.makeText(this,"Reparar vehículo seleccionado marca $marca para su Reparación",Toast.LENGTH_SHORT).show()
            alertDialog.dismiss()
        }

        btnAperturarNo.setOnClickListener{
            alertDialog.dismiss()
        }

        return alertDialog
    }

}