package com.academiamoviles.sesion03app

data class Vehiculo(
    val nombre:String,
    val anio:String,
    val descripcion:String,
    val marca:String)